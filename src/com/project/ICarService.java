package com.project;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICarService extends Remote {
	double computeTax(Car car) throws RemoteException;
	double computeSellingPrice(Car car) throws RemoteException;
}
