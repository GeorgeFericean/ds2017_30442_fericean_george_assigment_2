package com.project;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.awt.event.ActionEvent;

public class AppUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8449875333489610233L;
	private JPanel contentPane;
	

	private JTextField yearField;
	private JTextField engineSizeField;
	private JTextField priceField;
	private JTextArea response;

	/**
	 * Create the frame.
	 */
	public AppUI() {
		setTitle("Car Operations");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Enter car info");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(21, 11, 106, 24);
		contentPane.add(lblNewLabel);
		
		yearField = new JTextField();
		yearField.setBounds(21, 66, 86, 20);
		contentPane.add(yearField);
		yearField.setColumns(10);
		
		engineSizeField = new JTextField();
		engineSizeField.setBounds(21, 108, 86, 20);
		contentPane.add(engineSizeField);
		engineSizeField.setColumns(10);
		
		priceField = new JTextField();
		priceField.setBounds(21, 157, 86, 20);
		contentPane.add(priceField);
		priceField.setColumns(10);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblYear.setBounds(21, 46, 57, 14);
		contentPane.add(lblYear);
		
		JLabel lblEngineSize = new JLabel("Engine Size");
		lblEngineSize.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblEngineSize.setBounds(21, 91, 86, 14);
		contentPane.add(lblEngineSize);
		
		JLabel lblPurchasingPrice = new JLabel("Purchasing Price");
		lblPurchasingPrice.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPurchasingPrice.setBounds(21, 139, 106, 14);
		contentPane.add(lblPurchasingPrice);
		
		JButton btnNewButton = new JButton("Compute");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int year=Integer.parseInt(getYearField());
				int engineSize=Integer.parseInt(getEngineSizeField());
				double sellingPrice=Double.parseDouble(getPriceField());
				Car car= new Car(year,engineSize,sellingPrice);
				generateResponse(car);
			}
		});
		btnNewButton.setBounds(18, 188, 89, 23);
		contentPane.add(btnNewButton);
		
		response = new JTextArea();
		response.setBounds(172, 42, 240, 188);
		contentPane.add(response);
	}
	public String getYearField() {
		return yearField.getText();
	}

	public void setYearField(JTextField yearField) {
		this.yearField = yearField;
	}

	public String getEngineSizeField() {
		return engineSizeField.getText();
	}

	public void setEngineSizeField(JTextField engineSizeField) {
		this.engineSizeField = engineSizeField;
	}

	public String getPriceField() {
		return priceField.getText();
	}

	public void setPriceField(JTextField priceField) {
		this.priceField = priceField;
	}
	public void generateResponse(Car c) {
		 try {  
	         // Getting the registry 
	         Registry registry = LocateRegistry.getRegistry(null); 
	    
	         // Looking up the registry for the remote object 
	         ICarService stub = (ICarService) registry.lookup("CarInterface"); 
	         String responseString="Tax value: "+stub.computeTax(c)+"\n"+"Selling price value: "+stub.computeSellingPrice(c);
	         response.setText(responseString);
	        
	      } catch (Exception e) {
	         System.err.println("Client exception: " + e.toString()); 
	         e.printStackTrace(); 
	      } 
	   } 
}
