package com.project;

import java.rmi.RemoteException;

public class CarService implements ICarService {

	public double computeTax(Car c) {
		// Dummy formula
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(c.getEngineCapacity() > 1601) sum = 18;
		if(c.getEngineCapacity() > 2001) sum = 72;
		if(c.getEngineCapacity() > 2601) sum = 144;
		if(c.getEngineCapacity() > 3001) sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}

	public double computeSellingPrice(Car car) throws RemoteException {
		double purchasePrice=car.getPurchasingPrice();
		if (purchasePrice <= 0) {
			throw new IllegalArgumentException("purchasing price must be positive.");
		}
 
		double sellingPrice=purchasePrice- (purchasePrice*(2015-car.getYear()))/7;
		return sellingPrice;
	}

	
}
